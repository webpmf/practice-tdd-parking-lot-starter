package com.parkinglot;

public class UnRecognizedTicketException extends RuntimeException{
    public UnRecognizedTicketException(){
        super("UnRecognized parking ticket");
    }
}
