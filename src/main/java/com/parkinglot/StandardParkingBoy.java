package com.parkinglot;

import java.util.List;

public class StandardParkingBoy extends ParkingBoy {

    public StandardParkingBoy(List<ParkingLot> parkingLots, boolean isPark,boolean isFetch) {
        super(parkingLots, isPark,isFetch);
    }

    @Override
    public Ticket park(Car car) {
        if (!isPark) {
            throw new NoOperationException();
        }
        return parkingLots.stream().filter(ParkingLot::isAvailable).
                findFirst().map(parkingLot -> parkingLot.park(car)).orElseThrow(UnAvailablePosition::new);
    }

}
