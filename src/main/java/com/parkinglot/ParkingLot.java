package com.parkinglot;

import java.util.HashMap;
import java.util.Map;

public class ParkingLot {
    public int totalPositions;

    private final Map<Ticket,Car> carTicket;


    public ParkingLot(int totalPosition) {
        carTicket= new HashMap<>();
        this.totalPositions = totalPosition;
    }

    public Ticket park(Car car) {
        if(carTicket.size()>totalPositions){
            throw new UnAvailablePosition();
        }
        Ticket ticket = new Ticket();
        carTicket.put(ticket,car);
        return ticket;
    }

    public boolean isAvailable(){
        return carTicket.size()<=totalPositions;
    }

    public int availablePosition(){
        return totalPositions-carTicket.size();
    }

    public boolean haveTicket(Ticket ticket){
        return carTicket.containsKey(ticket);
    }

    public Car fetch(Ticket ticket) {
        if (!carTicket.containsKey(ticket)) {
            throw new UnRecognizedTicketException();
        }
        return carTicket.remove(ticket);

    }
}
