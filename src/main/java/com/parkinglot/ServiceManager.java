package com.parkinglot;

import java.util.ArrayList;
import java.util.List;

public class ServiceManager extends StandardParkingBoy{
    private final List<ParkingBoy> parkingBoys = new ArrayList<>();;
    public ServiceManager(List<ParkingLot> parkingLots,boolean isPark,boolean isFetch) {
        super(parkingLots,isPark,isFetch);
    }

    public List<ParkingBoy> getParkingBoys() {
        return parkingBoys;
    }

    public List<ParkingBoy> addParkingBoys(ParkingBoy parkingBoy) {
        parkingBoys.add(parkingBoy);
        return parkingBoys;
    }

    public Ticket assignParkingBoyPark(ParkingBoy assignParkingBoy,Car car) {
        return assignParkingBoy.park(car);
    }

    public Car assignParkingBoyFetch(ParkingBoy assignParkingBoy, Ticket ticket) {
        return assignParkingBoy.fetch(ticket);
    }
}
