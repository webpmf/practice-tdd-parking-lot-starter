package com.parkinglot;

public class NoOperationException extends RuntimeException{
    public NoOperationException(){
        super("No operation");
    }
}
