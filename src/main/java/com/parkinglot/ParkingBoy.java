package com.parkinglot;

import java.util.List;

public class ParkingBoy {

    public List<ParkingLot> parkingLots;
    public boolean isPark;

    public boolean isFetch;

    public ParkingBoy(List<ParkingLot> parkingLots,boolean isPark,boolean isFetch) {
        this.parkingLots = parkingLots;
        this.isPark = isPark;
        this.isFetch = isFetch;
    }

    public List<ParkingLot> getParkingLots() {
        return parkingLots;
    }

    public  Ticket park(Car car){
        return null;
    }

    public Car fetch(Ticket ticket) {
        if (!isFetch) {
            throw new NoOperationException();
        }
        return parkingLots.stream().filter(parkingLot -> parkingLot.haveTicket(ticket)).
                findFirst().map(parkingLot -> parkingLot.fetch(ticket)).orElseThrow(UnRecognizedTicketException::new);
    }
}
