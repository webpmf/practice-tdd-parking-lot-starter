package com.parkinglot;

public class UnAvailablePosition extends RuntimeException{
    public UnAvailablePosition(){
        super("No available position");
    }
}
