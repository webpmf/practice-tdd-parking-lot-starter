package com.parkinglot;

import java.util.List;

public class SmartParkingBoy extends ParkingBoy{


    public SmartParkingBoy(List<ParkingLot> parkingLots,boolean isPark,boolean isFetch) {
        super(parkingLots,isPark,isFetch);
    }

    @Override
    public Ticket park(Car car) {
        if (!isPark) {
            throw new NoOperationException();
        }
        return parkingLots.stream().reduce((parkingLo1,parkingLo2)->
                        parkingLo1.availablePosition()> parkingLo2.availablePosition()?
                                parkingLo1:parkingLo2).
                map(parkingLot -> parkingLot.park(car)).orElseThrow(UnAvailablePosition::new);
    }

}

