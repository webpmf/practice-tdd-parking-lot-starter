package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class ServiceManagerTest {
    @Test
    void should_return_ticket_when_park_car_given_a_parkingLot_and_a_serviceManager_and_a_car() {
        //given
        Car car = new Car();
        List<ParkingLot> parkingLots = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        ServiceManager serviceManager = new ServiceManager(parkingLots,true,true);

        //when
        parkingLot2.park(car);
        Ticket ticket = serviceManager.park(car);

        //then
        Assertions.assertTrue(parkingLot1.haveTicket(ticket));
        Assertions.assertFalse(parkingLot2.haveTicket(ticket));
    }

    @Test
    void should_add_parkingBoy_to_when_addParkingBoys_given_parkingBoys_and_a_serviceManger_and_parkingLots() {
        //given
        List<ParkingLot> parkingLots1 = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        parkingLots1.add(parkingLot1);
        parkingLots1.add(parkingLot2);
        List<ParkingLot> parkingLots2 = new ArrayList<>();
        ParkingLot parkingLot3 = new ParkingLot(10);
        ParkingLot parkingLot4 = new ParkingLot(10);
        parkingLots2.add(parkingLot3);
        parkingLots2.add(parkingLot4);
        ServiceManager serviceManager = new ServiceManager(parkingLots1,true,true);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLots2,true,true);

        //when
        List<ParkingBoy> parkingBoys = serviceManager.addParkingBoys(standardParkingBoy);

        //then
        Assertions.assertTrue(parkingBoys.contains(standardParkingBoy));

    }

    @Test
    void should_parkingBoy_park_car_parkingLot_when_assign_parkingBoy_park_car_given_a_parkingLot_and_a_serviceManager_and_a_car() {
        //given
        List<ParkingLot> parkingLots1 = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        parkingLots1.add(parkingLot1);
        parkingLots1.add(parkingLot2);
        List<ParkingLot> parkingLots2 = new ArrayList<>();
        ParkingLot parkingLot3 = new ParkingLot(10);
        ParkingLot parkingLot4 = new ParkingLot(10);
        parkingLots2.add(parkingLot3);
        parkingLots2.add(parkingLot4);
        ServiceManager serviceManager = new ServiceManager(parkingLots1,true,true);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLots2,true,true);
        serviceManager.addParkingBoys(standardParkingBoy);
        ParkingBoy assignParkingBoy = serviceManager.getParkingBoys().stream().
                findFirst().filter(parkingBoy -> parkingBoy == standardParkingBoy).get();

        //when
        Ticket ticket = serviceManager.assignParkingBoyPark(assignParkingBoy,new Car());

        //then
        Assertions.assertTrue(parkingLot3.haveTicket(ticket));
    }

    @Test
    void should_return_error_message_when_assign_parkingBoy_park_car_no_operation_and_given_a_parkingLot_and_a_serviceManager_and_a_car() {
        //given
        List<ParkingLot> parkingLots1 = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        parkingLots1.add(parkingLot1);
        parkingLots1.add(parkingLot2);
        List<ParkingLot> parkingLots2 = new ArrayList<>();
        ParkingLot parkingLot3 = new ParkingLot(10);
        ParkingLot parkingLot4 = new ParkingLot(10);
        parkingLots2.add(parkingLot3);
        parkingLots2.add(parkingLot4);
        ServiceManager serviceManager = new ServiceManager(parkingLots1,true,true);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLots2,false,true);
        serviceManager.addParkingBoys(standardParkingBoy);
        ParkingBoy assignParkingBoy = serviceManager.getParkingBoys().stream().
                findFirst().filter(parkingBoy -> parkingBoy == standardParkingBoy).get();

        //when
        //then
       NoOperationException noOperationException = Assertions.
                assertThrows(NoOperationException.class,()-> serviceManager.assignParkingBoyPark(assignParkingBoy,new Car()));
        Assertions.assertEquals("No operation",noOperationException.getMessage());
    }

    @Test
    void should_parkingBoy_fetch_car_when_assign_parkingBoy_fetch_car_given_a_parkingLot_and_a_serviceManager_and_a_ticket() {
        //given
        List<ParkingLot> parkingLots1 = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        parkingLots1.add(parkingLot1);
        parkingLots1.add(parkingLot2);
        List<ParkingLot> parkingLots2 = new ArrayList<>();
        ParkingLot parkingLot3 = new ParkingLot(10);
        ParkingLot parkingLot4 = new ParkingLot(10);
        parkingLots2.add(parkingLot3);
        parkingLots2.add(parkingLot4);
        ServiceManager serviceManager = new ServiceManager(parkingLots1,true,true);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLots2,true,true);
        serviceManager.addParkingBoys(standardParkingBoy);
        Car car = new Car();
        ParkingBoy assignParkingBoy = serviceManager.getParkingBoys().stream().
                findFirst().filter(parkingBoy -> parkingBoy == standardParkingBoy).get();
        Ticket ticket = serviceManager.assignParkingBoyPark(assignParkingBoy,car);

        //when
        Car fetchedCar = serviceManager.assignParkingBoyFetch(assignParkingBoy,ticket);

        //then
        Assertions.assertEquals(car,fetchedCar);
    }

    @Test
    void should_return_error_message_when_assign_parkingBoy_fetch_car_no_operation_and_given_a_parkingLot_and_a_serviceManager_and_a_car() {
        //given
        List<ParkingLot> parkingLots1 = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        parkingLots1.add(parkingLot1);
        parkingLots1.add(parkingLot2);
        List<ParkingLot> parkingLots2 = new ArrayList<>();
        ParkingLot parkingLot3 = new ParkingLot(10);
        ParkingLot parkingLot4 = new ParkingLot(10);
        parkingLots2.add(parkingLot3);
        parkingLots2.add(parkingLot4);
        ServiceManager serviceManager = new ServiceManager(parkingLots1,true,true);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLots2,true,false);
        serviceManager.addParkingBoys(standardParkingBoy);
        ParkingBoy assignParkingBoy = serviceManager.getParkingBoys().stream().
                findFirst().filter(parkingBoy -> parkingBoy == standardParkingBoy).get();
        Ticket ticket = serviceManager.assignParkingBoyPark(assignParkingBoy,new Car());

        //when
        //then
        NoOperationException noOperationException = Assertions.
                assertThrows(NoOperationException.class, () -> serviceManager.assignParkingBoyFetch(assignParkingBoy,ticket));
        Assertions.assertEquals("No operation",noOperationException.getMessage());
    }



}
