package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;


public class SuperSmartParkingBoyTest {
    @Test
    void should_return_park_in_parkLot1_when_park_car_given_parkingLot2_greater_than_parkingLot1_and_a_superSmartParkingBoy_and_a_car() {
        //given
        Car car = new Car();
        List<ParkingLot> parkingLots = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(100);
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        SuperSmartParkingBoy  superSmartParkingBoy = new SuperSmartParkingBoy(parkingLots,true,true);


        //when
        for (int i =0;i<11;i++) {
            parkingLot1.park(new Car());
        }
        Ticket ticket = superSmartParkingBoy.park(car);

        //then
        Assertions.assertTrue(parkingLot2.haveTicket(ticket));
        Assertions.assertFalse(parkingLot1.haveTicket(ticket));
    }

    @Test
    void should_return_park_in_parkingLot2_when_park_car_given_parkingLot1_is_full_and_a_parkingBoy_and_a_car() {
        //given
        Car car = new Car();
        List<ParkingLot> parkingLots = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        SuperSmartParkingBoy  superSmartParkingBoy = new SuperSmartParkingBoy(parkingLots,true,true);
        for (int i =0;i<11;i++) {
            parkingLot1.park(new Car());
        }

        //when
        Ticket ticket = superSmartParkingBoy.park(car);

        //then
        Assertions.assertTrue(parkingLot2.haveTicket(ticket));
        Assertions.assertFalse(parkingLot1.haveTicket(ticket));
    }

    @Test
    void should_return_car_when_fetch_car_given_a_parkingLot_and_a_ticket_and_a_parkingBoy() {
        //given
        Car car = new Car();
        List<ParkingLot> parkingLots = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        SuperSmartParkingBoy  superSmartParkingBoy = new SuperSmartParkingBoy(parkingLots,true,true);
        Ticket ticket = superSmartParkingBoy.park(car);

        //when
        Car fetchedCar = superSmartParkingBoy.fetch(ticket);

        //then
        Assertions.assertEquals(car,fetchedCar);
    }

    @Test
    void should_return_right_car_when_fetch_car_given_a_parkingLot_and_two_ticket_and_a_parkingBoy() {
        //given
        Car car1 = new Car();
        Car car2 = new Car();
        List<ParkingLot> parkingLots = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        SuperSmartParkingBoy  superSmartParkingBoy = new SuperSmartParkingBoy(parkingLots,true,true);
        Ticket ticket1 = parkingLot1.park(car1);
        Ticket ticket2 = parkingLot2.park(car2);

        //when
        Car fetchedCar1 = superSmartParkingBoy.fetch(ticket1);
        Car fetchedCar2 = superSmartParkingBoy.fetch(ticket2);

        //then
        Assertions.assertEquals(car1,fetchedCar1);
        Assertions.assertEquals(car2,fetchedCar2);
    }

    @Test
    void should_return_nothing_when_fetch_car_given_a_parkingLot_and_a_wrong_ticket_and_a_parkingBoy() {
        //given
        List<ParkingLot> parkingLots = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        SuperSmartParkingBoy  superSmartParkingBoy = new SuperSmartParkingBoy(parkingLots,true,true);

        //when
        //then
        UnRecognizedTicketException unRecognizedTicketException = Assertions.
                assertThrows(UnRecognizedTicketException.class,()-> superSmartParkingBoy.fetch(new Ticket()));
        Assertions.assertEquals("UnRecognized parking ticket",unRecognizedTicketException.getMessage());

    }

    @Test
    void should_return_nothing_when_fetch_car_given_a_parkingLot_and_a_used_ticket_and_a_parkingBoy() {
        //given
        Car car = new Car();
        List<ParkingLot> parkingLots = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        SuperSmartParkingBoy  superSmartParkingBoy = new SuperSmartParkingBoy(parkingLots,true,true);
        Ticket ticket = superSmartParkingBoy.park(car);
        superSmartParkingBoy.fetch(ticket);

        //when
        //then
        UnRecognizedTicketException unRecognizedTicketException = Assertions.
                assertThrows(UnRecognizedTicketException.class,()-> superSmartParkingBoy.fetch(ticket));
        Assertions.assertEquals("UnRecognized parking ticket",unRecognizedTicketException.getMessage());
    }

    @Test
    void should_return_message_when_fetch_car_given_a_parkingLot_has_no_position() {
        //given
        List<ParkingLot> parkingLots = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        for (int i =0;i<11;i++) {
           parkingLot1.park(new Car());
        }
        for (int i =0;i<11;i++) {
            parkingLot2.park(new Car());
        }
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLots,true,true);

        //when
        //then
        UnAvailablePosition unAvailablePosition = Assertions.
                assertThrows(UnAvailablePosition.class,()-> superSmartParkingBoy.park(new Car()));
        Assertions.assertEquals("No available position", unAvailablePosition.getMessage());

    }
}

