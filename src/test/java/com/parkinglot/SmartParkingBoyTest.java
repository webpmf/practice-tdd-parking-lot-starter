package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;


public class SmartParkingBoyTest {
    @Test
    void should_return_park_in_parkLot1_when_park_car_given_parkingLot1_greater_than_parkingLot2_and_a_SmartParkingBoy_and_a_car() {
        //given
        Car car = new Car();
        List<ParkingLot> parkingLots = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLots,true,true);

        //when
        parkingLot2.park(car);
        Ticket ticket = smartParkingBoy.park(car);

        //then
        Assertions.assertTrue(parkingLot1.haveTicket(ticket));
        Assertions.assertFalse(parkingLot2.haveTicket(ticket));
    }

    @Test
    void should_return_park_in_parkingLot2_when_park_car_given_parkingLot1_is_full_and_a_parkingBoy_and_a_car() {
        //given
        Car car = new Car();
        List<ParkingLot> parkingLots = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        SmartParkingBoy SmartParkingBoy = new SmartParkingBoy(parkingLots,true,true);
        for (int i =0;i<11;i++) {
           parkingLot1.park(new Car());
        }

        //when
        Ticket ticket = SmartParkingBoy.park(car);

        //then
        Assertions.assertTrue(parkingLot2.haveTicket(ticket));
        Assertions.assertFalse(parkingLot1.haveTicket(ticket));
    }

    @Test
    void should_return_car_when_fetch_car_given_a_parkingLot_and_a_ticket_and_a_parkingBoy() {
        //given
        Car car = new Car();
        List<ParkingLot> parkingLots = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        SmartParkingBoy SmartParkingBoy = new SmartParkingBoy(parkingLots,true,true);
        Ticket ticket = SmartParkingBoy.park(car);

        //when
        Car fetchedCar = SmartParkingBoy.fetch(ticket);

        //then
        Assertions.assertEquals(car,fetchedCar);
    }

    @Test
    void should_return_right_car_when_fetch_car_given_a_parkingLot_and_two_ticket_and_a_parkingBoy() {
        //given
        Car car1 = new Car();
        Car car2 = new Car();
        List<ParkingLot> parkingLots = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        SmartParkingBoy SmartParkingBoy = new SmartParkingBoy(parkingLots,true,true);
        Ticket ticket1 = parkingLot1.park(car1);
        Ticket ticket2 = parkingLot2.park(car2);

        //when
        Car fetchedCar1 = SmartParkingBoy.fetch(ticket1);
        Car fetchedCar2 = SmartParkingBoy.fetch(ticket2);

        //then
        Assertions.assertEquals(car1,fetchedCar1);
        Assertions.assertEquals(car2,fetchedCar2);
    }

    @Test
    void should_return_nothing_when_fetch_car_given_a_parkingLot_and_a_wrong_ticket_and_a_parkingBoy() {
        //given
        List<ParkingLot> parkingLots = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        SmartParkingBoy SmartParkingBoy = new SmartParkingBoy(parkingLots,true,true);

        //when
        //then
        UnRecognizedTicketException unRecognizedTicketException = Assertions.
                assertThrows(UnRecognizedTicketException.class,()-> SmartParkingBoy.fetch(new Ticket()));
        Assertions.assertEquals("UnRecognized parking ticket",unRecognizedTicketException.getMessage());

    }

    @Test
    void should_return_nothing_when_fetch_car_given_a_parkingLot_and_a_used_ticket_and_a_parkingBoy() {
        //given
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot(10);
        List<ParkingLot> parkingLots = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        SmartParkingBoy SmartParkingBoy = new SmartParkingBoy(parkingLots,true,true);
        Ticket ticket = SmartParkingBoy.park(car);
        SmartParkingBoy.fetch(ticket);

        //when
        //then
        UnRecognizedTicketException unRecognizedTicketException = Assertions.
                assertThrows(UnRecognizedTicketException.class,()-> SmartParkingBoy.fetch(ticket));
        Assertions.assertEquals("UnRecognized parking ticket",unRecognizedTicketException.getMessage());
    }

    @Test
    void should_return_message_when_fetch_car_given_a_parkingLot_has_no_position() {
        //given
        List<ParkingLot> parkingLots = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        SmartParkingBoy SmartParkingBoy = new SmartParkingBoy(parkingLots,true,true);
        for (int i =0;i<22;i++) {
           SmartParkingBoy.park(new Car());
        }

        //when
        //then
        UnAvailablePosition unAvailablePosition = Assertions.
                assertThrows(UnAvailablePosition.class,()-> SmartParkingBoy.park(new Car()));
        Assertions.assertEquals("No available position", unAvailablePosition.getMessage());

    }
}

