package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ParkingLotTest {
    @Test
    void should_return_ticket_when_park_car_given_a_parkingLot_and_a_car() {
        //given
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot(10);

        //when
        Ticket ticket = parkingLot.park(car);

        //then
        Assertions.assertNotNull(ticket);
    }

    @Test
    void should_return_car_when_fetch_car_given_a_parkingLot_and_a_ticket() {
        //given
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot(10);
        Ticket ticket = parkingLot.park(car);

        //when
        Car fetchedCar = parkingLot.fetch(ticket);

        //then
        Assertions.assertEquals(car,fetchedCar);
    }

    @Test
    void should_return_right_car_when_fetch_car_given_a_parkingLot_and_two_ticket() {
        //given
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingLot parkingLot = new ParkingLot(10);
        Ticket ticket1 = parkingLot.park(car1);
        Ticket ticket2 = parkingLot.park(car2);

        //when
        Car fetchedCar1 = parkingLot.fetch(ticket1);
        Car fetchedCar2 = parkingLot.fetch(ticket2);

        //then
        Assertions.assertEquals(car1,fetchedCar1);
        Assertions.assertEquals(car2,fetchedCar2);
    }

    @Test
    void should_return_nothing_when_fetch_car_given_a_parkingLot_and_a_wrong_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);

        //when
        //then
        UnRecognizedTicketException unRecognizedTicketException = Assertions.
                assertThrows(UnRecognizedTicketException.class,()->parkingLot.fetch(new Ticket()));
        Assertions.assertEquals("UnRecognized parking ticket",unRecognizedTicketException.getMessage());

    }

    @Test
    void should_return_nothing_when_fetch_car_given_a_parkingLot_and_a_used_ticket() {
        //given
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot(10);
        Ticket ticket = parkingLot.park(car);
        parkingLot.fetch(ticket);

        //when
        //then
        UnRecognizedTicketException unRecognizedTicketException = Assertions.
                assertThrows(UnRecognizedTicketException.class,()->parkingLot.fetch(ticket));
        Assertions.assertEquals("UnRecognized parking ticket",unRecognizedTicketException.getMessage());
    }

    @Test
    void should_return_message_when_fetch_car_given_a_parkingLot_has_no_position() {
        //given

        ParkingLot parkingLot = new ParkingLot(10);
        for (int i =0;i<11;i++) {
            parkingLot.park(new Car());
        }

        //when
        //then
        UnAvailablePosition unAvailablePosition = Assertions.
                assertThrows(UnAvailablePosition.class,()->parkingLot.park(new Car()));
        Assertions.assertEquals("No available position", unAvailablePosition.getMessage());
    }
}
